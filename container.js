var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function(){

if(xhr.readyState === 4 && xhr.status === 200){
var containerparams = JSON.parse(xhr.responseText);
var temp = [];
var moisture_content = [];
var time = [];

for(var i = 0; i < containerparams.length; i++){

time.push(containerparams[i].Time);

temp.push(containerparams[i].Temperature);
var tempValues = {
           labels: time,
           datasets: [{
                   label: "Varation of Temperature in the container with time",
                   fill: false,
                   lineTension: 0.1,
                   backgroundColor: "rgba(60,90,153,0.75)",
                   borderColor: "rgba(59,89,152,1)",
                   pointHoverBackgroundColor: "rgba(60,90,153,1)",
                   pointHoverBorderColor: "rgba(60,90,152,1)",
                   data: temp
}]
}


var temperature = document.getElementById("Temperature");
var tempChart  = new Chart(temperature,{
                 type: 'line',
		 width: 1000,
                 data:  tempValues,
		 options: {
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Temperature',
	    maxRotation: 360,
           },
	ticks: {
	  maxRotation: 360,
	  minRotation: 360
        }
        }],
	 xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Time'
          },
        ticks: {
	  autoSkip: false,
          maxRotation: 360,
          minRotation: 360
        }
        }]

      }
    }
});
tempChart.render();
temperature.innerHTML = tempChart;

moisture_content.push(containerparams[i].Humidity);
var moistureValues = {
           labels: time,
           datasets: [{
                   label: "Variation of moisture content in the container with time",
                   fill: false,
                   lineTension: 0.1,
                   backgroundColor: "rgba(60,153,90,0.75)",
                   borderColor: "rgba(59,152,89,1)",
                   pointHoverBackgroundColor: "rgba(60,153,90,1)",
                   pointHoverBorderColor: "rgba(60,152,90,1)",
                   data: moisture_content
}]
}


var humidity = document.getElementById("Humidity");
var humidityChart  = new Chart(humidity,{
                 type: 'line',
                 data:  moistureValues,
		   options: {
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Humidity',
            maxRotation: 360,
           },
        ticks: {
          maxRotation: 360,
          minRotation: 360
        }
        }],
         xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Time'
          },
        ticks: {
          autoSkip: false,
          maxRotation: 360,
          minRotation: 360
        }
        }]

      }
    }

});
humidity.innerHTML = humidityChart;

}
}
}
xhr.open('GET',"http://10.5.26.26/containerdata.php");
xhr.send();

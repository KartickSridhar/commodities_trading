<?php include "containerparameters.php"; ?>
<!DOCTYPE html>
<html>
<head>
<title> Monitoring food containers </title>
<link rel= "stylesheet" href= "containermodel.css">
<link rel= "stylesheet" href= "responsivecontainer.css">
</head>
<body>

<header>
<h1 align="center"> Risk assesment tool for commodities trading </h1>
</header>

<section>
<div id= "containerparameters">

<ul id= "displayparams">

<li>
<canvas id="Temperature">
</canvas>
</li>

<li>
<canvas id="Humidity">
</canvas>
</li>

</ul>
</div>

<script src="Chart.js" type="text/javascript"></script>
<script src="container.js" type="text/javascript"></script>

<table align="center">

<thead>
<tr>
<th>Time in seconds </th>
<th> Spot price in dollars </th>
<th> Ideal temperature in centigrade </th>
<th> Ideal humidity (in %) </th>
<th> Temperature in centigrade </th>
<th> Humidity(in %) </th>
<th> Expected compounded price </th>
<th> Deprecation factor </th>
<th> Actual compounded Price </th>
</tr>
</thead>

<tbody>

<?php while ($row = $contValues->fetch()): ?>
                        <tr>
                            <td><?php echo htmlspecialchars($row['Time']); ?></td>
                            <td><?php echo htmlspecialchars($row['Spot_price']); ?></td>
                            <td><?php echo htmlspecialchars($row['Ideal_temp']); ?></td>
                            <td><?php echo htmlspecialchars($row['Ideal_humidity']); ?></td>
                            <td><?php echo htmlspecialchars($row['Temperature']); ?></td>
                            <td><?php echo htmlspecialchars($row['Humidity']); ?></td>
                            <td><?php echo htmlspecialchars($row['Expected_Compounded_price']); ?></td>
			    <td><?php echo htmlspecialchars($row['Deprecation_factor']); ?></td>
			    <td><?php echo htmlspecialchars($row['Actual_Compounded_price']); ?></td>

                        </tr>
<?php endwhile; ?>
</tbody>
</table>



</section>
<footer></footer>
</body>
</html>


from __future__ import division
from sense_hat import SenseHat
from pyfcm import FCMNotification
from time import sleep
import math
import time
import pymysql
import ystockquote                        #importing all the necessary modules

sense = SenseHat()
sense.clear()

Time = 0                                  #starting time
ideal_temp = 4.444                        #ideal value of temperature expressed in degree Celsius
ideal_humidity = 14                       #ideal value of humidity expressed as a percentage

interest_rate = 0.012                     #interest rate is 1.2%
rateOf_storage_cost = 0.05                #storage cost is expressed as a percentage rate i.e. 5 %
future_price = 0

K1 = 6.531
K2 = 0.159                                #define constants
K3 = 0.069

time_span_months = 3
commodity = "rice"

fcm_push = FCMNotification("AAAAvmoLqsk:APA91bFJbgvbu6A7GZuryrUfKfH8eZXfwsQhmjT73RfL7pEZNC0y5rdOZ-p-wK_BxCgAeXpvxO75T09z1n-ZYTMZMyrTe2uhwLOC-nZM_t6RcDSqhCd5P-11tYoHKLDEPZldwaqN4lVh");     #getting the server key for cloud messaging
device_token = "eLVEofkmEWc:APA91bF46Kjka_tTJozJ4MgsHD4sDz05Xxa6udtWuU3W9C3hGB7rLnGSKyVodYxn7n1q5Cbe4u_RMVG8z21hbl4mD7vaCmXcq0tJJa6VG8-bq582neqezRR6PKbdEP_hQZoBnSG5dJuk"                   #device_token of the storage owner
tablet_token = "fNSRvWGSrWg:APA91bEIm1kN6n6UFRS4K3CmfCXnSBdbOq5mkTP1scD-CdRkp79mxmL7BFIYphGXElNJXX9nN0SecDBExIwZU4zzDjAEIe1FrRWQhq_GxO0YOx-KnZrR9Efc4Cf6JvPFhqMBS4KOnedU"                   #this is the device_token of the investor or retailer
message_title = "Container alert"

while Time < 3180:
	temp = sense.get_temperature()                           #Inputs involve sensing the temperature and humidity values and obtaining the spot_price of the commodity
	moisture_content = sense.get_humidity()                  #sense temperature and humidity values of the rice container from the environment
	spot_price = ystockquote.get_last_trade_price("RR=F")    #get the spot_price of the commodity i.e. rice from the stock exchange

	temp = int(round(temp, 1))
	moisture_content = int(round(moisture_content, 1))
	compounded_price = float(spot_price) * float(math.exp((interest_rate + rateOf_storage_cost) * (Time/(24 * 3600 * 365))))
	half_life = 10 ** (K1 - moisture_content * K2 - temp * K3)

	db = pymysql.connect("localhost","rob","robby","containerParameters")
	cursor = db.cursor()

	if half_life > 90:
		deprecation_factor = 0
		future_minute_price = compounded_price
		future_price += future_minute_price
		cursor.execute("""INSERT INTO container VALUES(%s ,%s ,%s ,%s ,%s, %s, %s, %s, %s)""",(Time, spot_price, ideal_temp, ideal_humidity, temp, moisture_content, compounded_price, deprecation_factor, future_minute_price))
                db.commit()
        else:
		deprecation_factor = half_life/90
		future_minute_price = compounded_price - (deprecation_factor * compounded_price)
		future_price += future_minute_price
		cursor.execute("""INSERT INTO container VALUES(%s ,%s ,%s ,%s ,%s, %s, %s, %s, %s)""",(Time, spot_price, ideal_temp, ideal_humidity, temp, moisture_content, compounded_price, deprecation_factor, future_minute_price))
                db.commit()

		if temp > ideal_temp:
        		if temp == 15.556 and moisture_content <> 13:
				warning = "Inadequate moisture content at 60 degree Farenheit, prone to mold growth"
			        warningmessg = fcm_push.notify_single_device(device_token, message_title, warning)
				print warningmessg

			elif temp == 26.667 and moisture_content <> 12.5:
				warn = "Inadequate moisture content at 80 degree Farenheit, prone to mold growth"
		                warningmessage = fcm_push.notify_single_device(device_token, message_title, warn)
			        print warningmessage

			else:
				alert = "Temperature in the container is too high with inadequate moisture content"
                        	alertmessage = fcm_push.notify_single_device(device_token, message_title, alert)
				print alertmessage

		elif temp <= ideal_temp:
			if moisture_content > ideal_humidity:
				excess_moisture_message = "Excess of moisture might lead to mold growth"                 #poor quality
				excess_moisture_warningmessg = fcm_push.notify_single_device(device_token, message_title, excess_moisture_message)
				print excess_moisture_warningmessg
			else:
				suggest = "Moisture content is too low, it can be slightly increased"   #medium quality
				suggestion = fcm_push.notify_single_device(device_token, message_title, suggest)
				print suggestion

	msg = "Temperature = {0},humidity = {1}".format(temp,moisture_content)
        print (msg)
	sleep(120)
	Time = Time + 120
average_future_price = future_price/27
notification_alert = "The future price of " + commodity + " is expected to be " +str(average_future_price) 
notification = fcm_push.notify_single_device(tablet_token, "Commodity future price update",notification_alert)
print notification





